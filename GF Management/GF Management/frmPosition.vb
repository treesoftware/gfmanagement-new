﻿Public Class frmPosition

    Private Sub frmPosition_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        position()
        butEdit.Enabled = False
    End Sub

    Private Sub position()
        Dim sqlconn As New MySqlConnection(ConnectionstringDB.sqcon)
        Dim cmd As New MySqlCommand("SELECT * FROM position", sqlconn)
        Dim DA As New MySqlDataAdapter(cmd)
        Dim DT As New DataTable
        DA.Fill(DT)
        dgvPosition.Rows.Clear()

        If DT.Rows.Count > 0 Then
            For i As Integer = 0 To DT.Rows.Count - 1
                dgvPosition.Rows.Add()
                dgvPosition.Rows(i).Cells(0).Value = DT.Rows(i).Item(0)
                dgvPosition.Rows(i).Cells(1).Value = DT.Rows(i).Item(1)
            Next
        End If
    End Sub

    Private Sub butSave_Click(sender As Object, e As EventArgs) Handles butSave.Click
        Try
            If txtPos_id.Text = "" Then
                MessageBox.Show("กรุณาใส่รหัสตำแหน่งด้วย", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            ElseIf txtPos_name.Text = "กรุณาใส่ชื่อตำแหน่งด้วย" Then
                MessageBox.Show("กรุณาใส่รหัสตำแหน่งด้วย", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("คุณต้องการที่จะบันทึกข้อมุลใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim sqlconn As New MySqlConnection(ConnectionstringDB.sqcon)
                Dim cmd As New MySqlCommand("INSERT INTO position VALUES(@p1,@p2)", sqlconn)
                cmd.Parameters.AddWithValue("@p1", txtPos_id.Text)
                cmd.Parameters.AddWithValue("@p2", txtPos_name.Text)
                Dim DA As New MySqlDataAdapter(cmd)
                sqlconn.Open()
                cmd.ExecuteNonQuery()
                sqlconn.Close()
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "บันทึกข้อมูล", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtPos_id.Clear()
                txtPos_name.Clear()
                position()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvPosition_DoubleClick(sender As Object, e As EventArgs) Handles dgvPosition.DoubleClick
        txtPos_id.Text = dgvPosition.CurrentRow.Cells(0).Value
        txtPos_name.Text = dgvPosition.CurrentRow.Cells(1).Value
        txtPos_id.ReadOnly = True
        butEdit.Enabled = True
        butSave.Enabled = False
    End Sub

    Private Sub butEdit_Click(sender As Object, e As EventArgs) Handles butEdit.Click
        Try
            If txtPos_name.Text = "" Then
                MessageBox.Show("กรุณาใส่ชื่อตำแหน่งด้วย", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("คุณต้องการที่จะแก้ไขข้อมุลใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim sqlconn As New MySqlConnection(ConnectionstringDB.sqcon)
                Dim cmd As New MySqlCommand("UPDATE position SET pos_name=@p1 WHERE pos_id=@p2", sqlconn)
                cmd.Parameters.AddWithValue("@p1", txtPos_name.Text)
                cmd.Parameters.AddWithValue("@p2", txtPos_id.Text)
                Dim DA As New MySqlDataAdapter(cmd)
                sqlconn.Open()
                cmd.ExecuteNonQuery()
                sqlconn.Close()
                MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", "บันทึกข้อมูล", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtPos_id.Clear()
                txtPos_name.Clear()
                position()
                butEdit.Enabled = False
                butSave.Enabled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub butDel_Click(sender As Object, e As EventArgs) Handles butDel.Click
        Try
            If MessageBox.Show("คุณต้องการที่จะลบข้อมุลใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim sqlconn As New MySqlConnection(ConnectionstringDB.sqcon)
                Dim cmd As New MySqlCommand("DELETE FROM position WHERE pos_id=@p1", sqlconn)
                cmd.Parameters.AddWithValue("@p1", dgvPosition.CurrentRow.Cells(0).Value)
                Dim DA As New MySqlDataAdapter(cmd)
                sqlconn.Open()
                cmd.ExecuteNonQuery()
                sqlconn.Close()
                MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", "บันทึกข้อมูล", MessageBoxButtons.OK, MessageBoxIcon.Information)
                position()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class