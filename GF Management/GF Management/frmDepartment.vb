﻿Imports MySql.Data.MySqlClient
Public Class frmDepartment

    Private Sub frmDepartment_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        butEdit.Enabled = False
        Try
            Dim sqlcon As New connectionstring
            Dim cmd As New MySqlCommand("SELECT * FROM department", sqlcon.sqlcon)
            Dim DA As New MySqlDataAdapter(cmd)
            Dim DT As New DataTable
            DA.Fill(DT)
            dgvDep.Rows.Clear()

            If DT.Rows.Count > 0 Then
                For i As Integer = 0 To DT.Rows.Count - 1
                    dgvDep.Rows.Add()
                    dgvDep.Rows(i).Cells(0).Value = DT.Rows(i).Item(0)
                    dgvDep.Rows(i).Cells(1).Value = DT.Rows(i).Item(1)
                    dgvDep.Rows(i).Cells(2).Value = DT.Rows(i).Item(2)
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub butSave_Click(sender As Object, e As EventArgs) Handles butSave.Click
        Try
            If txtIsEmpty(My.Forms.frmDepartment) Then
                MessageBox.Show("กรุณาใส่ข้อมูลให้ครบด้วย", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("คุณต้องการที่จะบันทึกข้อมุลใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim cmd As New MySqlCommand("INSERT INTO department(dep_id,dep_name,dep_comment) VALUES(@p1,@p2,@p3)", sqlcon)
                cmd.Parameters.AddWithValue("@p1", txtDepid.Text)
                cmd.Parameters.AddWithValue("@p2", txtDepname.Text)
                cmd.Parameters.AddWithValue("@p3", txtComment.Text)
                Dim DA As New MySqlDataAdapter(cmd)
                sqlcon.Open()
                cmd.ExecuteNonQuery()
                sqlcon.Close()
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "บันทึกข้อมูล", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtDepid.Clear()
                txtDepname.Clear()
                txtComment.Clear()
                butSave.Enabled = True
                frmDepartment_Load(Nothing, EventArgs.Empty)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvDep_DoubleClick(sender As Object, e As EventArgs) Handles dgvDep.DoubleClick
        Try
            butSave.Enabled = False
            butEdit.Enabled = True
            txtDepid.ReadOnly = True
            txtDepid.Text = dgvDep.CurrentRow.Cells(0).Value
            txtDepname.Text = dgvDep.CurrentRow.Cells(1).Value
            txtComment.Text = dgvDep.CurrentRow.Cells(2).Value
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub butEdit_Click(sender As Object, e As EventArgs) Handles butEdit.Click
        Try
            If txtDepname.Text = "" Then
                MessageBox.Show("กรุณาใส่ชื่อแผนกด้วย", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txtDepname.Focus()
                Exit Sub
            End If
            If MessageBox.Show("คุณต้องการที่จะแก้ไขข้อมุลใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim cmd As New MySqlCommand("UPDATE department SET dep_name=@p1,dep_comment=@p2 WHERE dep_id=@p3", sqlcon)
                cmd.Parameters.AddWithValue("@p1", txtDepname.Text)
                cmd.Parameters.AddWithValue("@p2", txtComment.Text)
                cmd.Parameters.AddWithValue("@p3", txtDepid.Text)
                Dim DA As New MySqlDataAdapter(cmd)
                sqlcon.Open()
                cmd.ExecuteNonQuery()
                sqlcon.Close()
                MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", "บันทึกข้อมูล", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtDepid.Clear()
                txtDepname.Clear()
                txtComment.Clear()
                txtDepid.ReadOnly = False
                butSave.Enabled = True
                frmDepartment_Load(Nothing, EventArgs.Empty)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub butDel_Click(sender As Object, e As EventArgs) Handles butDel.Click
        Try
            If MessageBox.Show("คุณต้องการที่จะลบข้อมุลใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim cmd As New MySqlCommand("DELETE FROM department WHERE dep_id=@p1", sqlcon)
                cmd.Parameters.AddWithValue("@p1", dgvDep.CurrentRow.Cells(0).Value.ToString)
                Dim DA As New MySqlDataAdapter(cmd)
                sqlcon.Open()
                cmd.ExecuteNonQuery()
                sqlcon.Close()
                MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", "บันทึกข้อมูล", MessageBoxButtons.OK, MessageBoxIcon.Information)
                frmDepartment_Load(Nothing, EventArgs.Empty)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub
End Class