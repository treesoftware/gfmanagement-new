﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPosition
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtPos_id = New System.Windows.Forms.TextBox()
        Me.butSave = New System.Windows.Forms.Button()
        Me.txtPos_name = New System.Windows.Forms.TextBox()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvPosition = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPosition, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.dgvPosition)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(6, 7)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.GroupBox2.Size = New System.Drawing.Size(991, 524)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "สร้างตำแหน่งงาน"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPos_id)
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.txtPos_name)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(20, 41)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(772, 166)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'txtPos_id
        '
        Me.txtPos_id.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPos_id.Location = New System.Drawing.Point(118, 32)
        Me.txtPos_id.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPos_id.Name = "txtPos_id"
        Me.txtPos_id.Size = New System.Drawing.Size(76, 29)
        Me.txtPos_id.TabIndex = 19
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(129, 99)
        Me.butSave.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(122, 29)
        Me.butSave.TabIndex = 8
        Me.butSave.Text = "บันทึกตำแหน่งงาน"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'txtPos_name
        '
        Me.txtPos_name.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPos_name.Location = New System.Drawing.Point(296, 32)
        Me.txtPos_name.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPos_name.Name = "txtPos_name"
        Me.txtPos_name.Size = New System.Drawing.Size(125, 29)
        Me.txtPos_name.TabIndex = 17
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(363, 99)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(58, 29)
        Me.butEdit.TabIndex = 9
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(281, 99)
        Me.butDel.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(58, 29)
        Me.butDel.TabIndex = 10
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(30, 33)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 26)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "รหัสตำแหน่ง :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(217, 35)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 26)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "ชื่อตำแหน่ง :"
        '
        'dgvPosition
        '
        Me.dgvPosition.AllowUserToAddRows = False
        Me.dgvPosition.BackgroundColor = System.Drawing.Color.White
        Me.dgvPosition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPosition.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column4})
        Me.dgvPosition.Location = New System.Drawing.Point(10, 227)
        Me.dgvPosition.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.dgvPosition.Name = "dgvPosition"
        Me.dgvPosition.RowTemplate.Height = 33
        Me.dgvPosition.Size = New System.Drawing.Size(782, 230)
        Me.dgvPosition.TabIndex = 4
        '
        'Column1
        '
        Me.Column1.HeaderText = "รหัสตำแหน่ง"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 150
        '
        'Column4
        '
        Me.Column4.HeaderText = "ชื่อตำแหน่ง"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 200
        '
        'frmPosition
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1008, 729)
        Me.Controls.Add(Me.GroupBox2)
        Me.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.Name = "frmPosition"
        Me.Text = "ตั้งค่าตำแหน่ง"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvPosition, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents butDel As Button
    Friend WithEvents butEdit As Button
    Friend WithEvents butSave As Button
    Friend WithEvents dgvPosition As DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPos_id As TextBox
    Friend WithEvents txtPos_name As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
End Class
