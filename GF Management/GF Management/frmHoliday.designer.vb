﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHoliday
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHol_date = New System.Windows.Forms.TextBox()
        Me.txtHol_comment = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvHol = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.butSave = New System.Windows.Forms.Button()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtHol_year = New System.Windows.Forms.TextBox()

        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()


        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()


        CType(Me.dgvHol, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(314, 66)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 22)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "วันหยุดประจำปี :"
        '
        'txtHol_date
        '
        Me.txtHol_date.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHol_date.Location = New System.Drawing.Point(408, 63)
        Me.txtHol_date.Margin = New System.Windows.Forms.Padding(2)
        Me.txtHol_date.Name = "txtHol_date"
        Me.txtHol_date.Size = New System.Drawing.Size(128, 29)
        Me.txtHol_date.TabIndex = 2
        '
        'txtHol_comment
        '
        Me.txtHol_comment.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHol_comment.Location = New System.Drawing.Point(408, 98)
        Me.txtHol_comment.Margin = New System.Windows.Forms.Padding(2)
        Me.txtHol_comment.Name = "txtHol_comment"
        Me.txtHol_comment.Size = New System.Drawing.Size(128, 29)
        Me.txtHol_comment.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(341, 101)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 22)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "หมายเหตุ :"
        '
        'dgvHol
        '
        Me.dgvHol.AllowUserToAddRows = False
        Me.dgvHol.BackgroundColor = System.Drawing.Color.White
        Me.dgvHol.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHol.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column4, Me.Column2, Me.Column5, Me.Column1, Me.Column3})
        Me.dgvHol.Location = New System.Drawing.Point(5, 212)
        Me.dgvHol.Margin = New System.Windows.Forms.Padding(2)
        Me.dgvHol.Name = "dgvHol"
        Me.dgvHol.RowTemplate.Height = 33
        Me.dgvHol.Size = New System.Drawing.Size(930, 209)
        Me.dgvHol.TabIndex = 8
        '
        'Column2
        '
        Me.Column2.HeaderText = "วัน/เดือน หยุด"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 200
        '
        'Column5
        '
        Me.Column5.HeaderText = "พ.ศ."
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 200
        '
        'Column3
        '
        Me.Column3.HeaderText = "หมายเหตุ"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 200
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.dgvHol)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtHol_year)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtHol_date)
        Me.GroupBox1.Controls.Add(Me.txtHol_comment)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(975, 426)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "กำหนดวันหยุดประจำปี"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(5, 28)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 32)
        Me.DateTimePicker1.TabIndex = 14
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(282, 149)
        Me.butSave.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(58, 29)
        Me.butSave.TabIndex = 11
        Me.butSave.Text = "บันทึก"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(453, 149)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(58, 29)
        Me.butEdit.TabIndex = 12
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(374, 149)
        Me.butDel.Margin = New System.Windows.Forms.Padding(2, 1, 2, 1)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(58, 29)
        Me.butDel.TabIndex = 13
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(363, 35)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 22)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "พ.ศ. :"
        '
        'txtHol_year
        '
        Me.txtHol_year.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHol_year.Location = New System.Drawing.Point(489, 31)
        Me.txtHol_year.Margin = New System.Windows.Forms.Padding(2)
        Me.txtHol_year.Name = "txtHol_year"
        Me.txtHol_year.Size = New System.Drawing.Size(128, 29)
        Me.txtHol_year.TabIndex = 9
        '

        'Column4
        '
        Me.Column4.HeaderText = "ลำดับ"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 50
        '
        'Column2
        '
        Me.Column2.HeaderText = "วัน/เดือน หยุด"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 200
        '
        'Column5
        '
        Me.Column5.HeaderText = "พ.ศ."
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 200
        '
        'Column1
        '
        Me.Column1.HeaderText = "ชื่อวันหยุด"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 200
        '
        'Column3
        '
        Me.Column3.HeaderText = "หมายเหตุ"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 200
        '

        'frmHoliday
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1008, 661)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmHoliday"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmSetupHoliday"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvHol, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents txtHol_date As TextBox
    Friend WithEvents txtHol_comment As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents dgvHol As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtHol_year As TextBox
    Friend WithEvents butSave As Button
    Friend WithEvents butEdit As Button
    Friend WithEvents butDel As Button
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
End Class
